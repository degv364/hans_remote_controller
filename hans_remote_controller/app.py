import argparse
import logging
from robot_interface import RobotInterface


def main():
    logging.basicConfig(
        format="%(asctime)s: %(levelname)s %(module)s %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S"
    )
    parser = argparse.ArgumentParser(
        description="Tritech temote hans controller"
    )
    parser.add_argument(
        "-a", "--robotaddr", dest="robotaddr",
        type=str, required=False,
        help="IP address of the robot t use"
    )

    args = parser.parse_args()

    robot_ip_address = args.robotaddr
    logging.info("Connecting to robot at address: %s", robot_ip_address)

    # FIXME: Verify that robot_ip_address is a valid ip address

    robot_interface = RobotInterface(robot_ip_address)
