import socket
import time
import logging
from numpy import array


class RobotInterface:
    def __init__(self,
                 ip_address,
                 robot_index=0,
                 force_update=True,
                 motion_monitoring_frequency=60):
        # Create the socket for communication
        self.control_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.control_socket.connect((ip_address, 10003))
        logging.INFO("Successful connection to robot Command socket")
        # Create the logs socket
        self.logs_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.logs_socket.connect((ip_address, 30003))
        logging.INFO("Successful connection to robot logs socket")

        self._robot_index = str(robot_index)
        self.force_update = force_update
        # Populate initial state conditions
        self._update_state()
        logging.INFO("Updated robot state info")

        self.motion_monitoring_frequency = motion_monitoring_frequency
        self.joint_pose = array([0]*6)  # J1, J2, J3, J4, J5, J6
        self.end_pose = array([0]*6)  # X, Y, Z, RX, RY, RZ
        self.tcp_pose = array([0]*6)  # X, Y, Z, RX, RY, RZ

    # Internal low level methods
    def _send(self, msg):
        # Send a command to the robot
        self.control_socket.send(str.encode(msg))
        logging.DEBUG("Sent msg to robot: %s", msg)
        # Get the response
        result = self.control_socket.recv(1024).decode("UTF-8")
        logging.DEBUG("Got msg from robot: %s", result)
        return result

    def _get_logs(self, msg):
        self.curent_logs = self.log_socket.recv(1024).decode("UTF-8")

    def _cmd(self, instruction, *args):
        # Send the current command to the robot
        msg = ",".join([instruction] + args) + ",;"
        response_str = self._send(msg)
        response = response_str.split(",")
        if response[0] != instruction:
            # FIXME: raise a proper exception
            raise RuntimeError(
                "The response does not correspond the the sent instruction")

        if response[1] == "Fail":
            # FIXME: raise the proper exception
            raise RuntimeError(
                "Instruction " + instruction + " Failed with error code: " +
                response[2])

        logging.INFO("Instruction %s successful", instruction)
        # Return the parameters if any
        return response[2:-1]

    def _moving_robot_bloking(self):
        """
        This function will keep monitoring the robot and block the current
        thread until the movement is completed.
        """
        self._update_state()
        while self._moving:
            time.sleep(1.0/self.motion_monitoring_frequency)
            logging.DEBUG("Blocking execution until motion finishes")
            self._update_state()
            if self._error():
                # FIXME: use a proper error
                raise RuntimeError(
                    "There was an error with the robot. Code: " +
                    str(self._error_code))

    def _update_state(self):
        """
        Get the current state of the robot
        """
        state = self._cmd("ReadRobotState", self._robot_index)
        self._moving = int(state[0]) == 1
        self._enabled = int(state[1]) == 1
        self._error = int(state[2]) == 1
        self._error_code = int(state[3])
        self._error_axis = int(state[4])
        self._braking = int(state[5]) == 1
        self._holding = int(state[6])
        self._emergency_stop = int(state[7]) == 1
        self._safety_guard = int(state[8]) == 1
        self._power = int(state[9]) == 1
        self._connnected_box = int(state[10]) == 1
        self._blending_done = int(state[11]) == 1
        self._in_position = int(state[12]) == 1

    def _update_current_pose(self):
        """
        Get the current joint and cartesian pose.
        """
        pose_msg = self._cmd("ReadActPos", self._robot_index)
        self.joint_pose = array([int(jnt) for jnt in pose_msg[0:6]])
        self.end_pose = array([int(pos) for pos in pose_msg[6:12]])
        self.tcp_pose = array([int(pos) for pos in pose_msg[12:18]])

    # ==== Decorators and other syntax sugar ==================================
    # Robot index
    @property
    def robot_index(self):
        return int(self._robot_index)

    @robot_index.setter
    def robot_index(self, value):
        self._robot_index = str(value)

    # RobotState.moving
    @property
    def moving(self):
        if self.force_update:
            self._update_state()
        return self._moving

    @moving.setter
    def moving(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.enabled
    @property
    def enabled(self):
        if self.force_update:
            self._update_state()
        return self._enabled

    @enabled.setter
    def enabled(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.error
    @property
    def error(self):
        if self.force_update:
            self._update_state()
        return self._error

    @error.setter
    def error(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.error_code
    @property
    def error_code(self):
        if self.force_update:
            self._update_state()
        return self._error_code

    @error_code.setter
    def error_code(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.error_axis
    @property
    def error_axis(self):
        if self.force_update:
            self._update_state()
        return self._error_axis

    @error_axis.setter
    def error_axis(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.braking
    @property
    def braking(self):
        if self.force_update:
            self._update_state()
        return self._braking

    @braking.setter
    def braking(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.holding
    @property
    def holding(self):
        if self.force_update:
            self._update_state()
        return self._holding

    @holding.setter
    def holding(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.emergency_stop
    @property
    def emergency_stop(self):
        if self.force_update:
            self._update_state()
        return self._emergency_stop

    @emergency_stop.setter
    def emergency_stop(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.safety_guard
    @property
    def safety_guard(self):
        if self.force_update:
            self._update_state()
        return self._safety_guard

    @safety_guard.setter
    def safety_guard(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.power
    @property
    def power(self):
        if self.force_update:
            self._update_state()
        return self._power

    @power.setter
    def power(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.connected_box
    @property
    def connected_box(self):
        if self.force_update:
            self._update_state()
        return self._connected_box

    @connected_box.setter
    def connected_box(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.blending_done
    @property
    def blending_done(self):
        if self.force_update:
            self._update_state()
        return self._blending_done

    @blending_done.setter
    def blending_done(self, value):
        raise RuntimeError("Robot states are read only")

    # RobotState.in_position
    @property
    def in_position(self):
        if self.force_update:
            self._update_state()
        return self._in_position

    @in_position.setter
    def in_position(self, value):
        raise RuntimeError("Robot states are read only")

    # User side instructions
    def read_inputs(self, input_group, indexes):
        """
        Read the value of he inputs
        :param input_group: Which inputs should the controller read.
        :param indexes: Specific inputs to read
        """

        # FIXME: make this a Enum
        instructions_map = {
            "End Digital Input": "ReadEI",
            "End Digital Output": "ReadEO",
            "End Analog Input": "ReadEAI",
            "Box Digital Input": "ReadBoxDI",
            "Box Digital Input Configurable": "ReadBoxCI",
            "Box Digital Output": "ReadBoxDO",
            "Box Digital Output Configurable": "ReadBoxCO",
            "Box Analog Input": "ReadBoxAI",
            "Box Analog Output": "ReadBoxAO"
        }
        if input_group not in instructions_map:
            # FIXME: raise the proper exception
            raise RuntimeError("Invalid instruction")

        states = []
        for index in list(indexes):
            states.append(self._cmd(instructions_map[input_group],
                                    self._robot_index, index))

        return states

    def set_output(self, output_group, index, value):
        """
        RIght now only works for end effector values
        :param output_group: Which output group to use
                             (digital, analog, end, box etc)
        :param indexe: which output to change
        :param value: value to set
        """
        # FIXME: implement other output groups
        instructions_map = {
            "End Digital Output": "SetEndDO"
        }

        if output_group not in instructions_map:
            raise NotImplementedError("We need to add this instruction: " +
                                      output_group)
        self._cmd(
            instructions_map[output_group],
            self._robot_index,
            str(index),
            str(value))

    def script_execution(self, action):
        """
        Start, stop, pause, and resume script execution
        :param action: start, stop, suspend, resume
        """

        instruction_map = {
            "Start": "StartScript",
            "Stop": "StopScript",
            "Pause": "PauseScript",
            "Resume": "ContinusScript"
        }

        self._cmd(instruction_map[action])

    def run_function(self, function_name, *params):
        """
        Run a specific function within the active script
        :param function_name: name of the function to run
        :param params: parameters of the function
        """
        self._cmd("RunFunc", function_name, params)

    def set_speed(self, speed):
        """
        Se the max speed limit
        """

        self._cmd("SetOverride", self._robot_index, str(speed))

    def joint_move(self, goal):
        self.joint_goal = array(goal)
        self._cmd(
            "MoveJ",
            self._robot_index,
            self.joint_goal[0],
            self.joint_goal[1],
            self.joint_goal[2],
            self.joint_goal[3],
            self.joint_goal[4],
            self.joint_goal[5])
        self._moving_robot_bloking()

    def cartesian_move(self, goal):
        self.cart_goal = array(goal)
        self._cmd(
            "MoveL",
            self._robot_index,
            self.joint_goal[0],
            self.joint_goal[1],
            self.joint_goal[2],
            self.joint_goal[3],
            self.joint_goal[4],
            self.joint_goal[5])
        self._moving_robot_bloking()
